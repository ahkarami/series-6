package sbu.cs.multithread.priority;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class Runner {

    public static List<Message> messages = new ArrayList<>();
    CountDownLatch blackLatch;
    CountDownLatch blueLatch;
    CountDownLatch whiteLatch;

    /**
     * add your codes to this function. this function is the caller function which will be called first.
     * changing other codes in this function is allowed.
     *
     * @param blackCount    number of black threads
     * @param blueCount     number of blue threads
     * @param whiteCount    number of white threads
     */
    public void run(int blackCount, int blueCount, int whiteCount) throws InterruptedException {
        List<ColorThread> colorThreads = new ArrayList<>();

        blackLatch = new CountDownLatch(blackCount);
        blueLatch = new CountDownLatch(blueCount);
        whiteLatch = new CountDownLatch(whiteCount);


        // your codes here

        for (int i = 0; i < blackCount; i++) {
            BlackThread blackThread = new BlackThread(blackLatch);
            colorThreads.add(blackThread);
            blackThread.start();
        }
        blackLatch.await();


        for (int i = 0; i < blueCount; i++) {
            BlueThread blueThread = new BlueThread(blueLatch);
            colorThreads.add(blueThread);
            blueThread.start();
        }
        blueLatch.await();


        for (int i = 0; i < whiteCount; i++) {
            WhiteThread whiteThread = new WhiteThread(whiteLatch);
            colorThreads.add(whiteThread);
            whiteThread.start();
        }
        whiteLatch.await();


        // your codes here

    }

    synchronized public static void addToList(Message message) {
        messages.add(message);
    }

    public List<Message> getMessages() {
        return messages;
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

    }
}
