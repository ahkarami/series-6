package sbu.cs.multithread.semaphor;

import java.util.concurrent.Semaphore;

public class Chef extends Thread {

    Semaphore semaphore;

    public Chef(String name, Semaphore semaphore) {
        super(name);
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {

            try {
                sleep(1000);
                System.out.println(super.getName() + " is waiting for permission");
                semaphore.acquire();
                System.out.println(super.getName() + " got permission" + " i = " + i );
                Source.getSource();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(super.getName() + " is releasing permission");
            semaphore.release();

        }
    }
}
