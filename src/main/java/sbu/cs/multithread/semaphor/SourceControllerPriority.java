package sbu.cs.multithread.semaphor;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Semaphore;

public class SourceControllerPriority {

    public static void main(String[] args) throws InterruptedException{
        Semaphore semaphore = new Semaphore(2);

        ArrayBlockingQueue<Chef> chefs = new ArrayBlockingQueue<>(5);




        Chef chef1 = new Chef("chef1", semaphore);
        Chef chef2 = new Chef("chef2", semaphore);
        Chef chef3 = new Chef("chef3", semaphore);
        Chef chef4 = new Chef("chef4", semaphore);
        Chef chef5 = new Chef("chef5", semaphore);

        chefs.put(chef1);
        chefs.put(chef2);
        chefs.put(chef3);
        chefs.put(chef4);
        chefs.put(chef5);


        chef1.setPriority(1);
        chef2.setPriority(2);
        chef3.setPriority(1);
        chef4.setPriority(3);
        chef5.setPriority(1);

        for (int i = 0; i < 5; i++) {
            chefs.take().start();
            System.out.println(chefs.remainingCapacity());
        }

//        chef1.start();
//        chef2.start();
//        chef3.start();
//        chef4.start();
//        chef5.start();
    }
}
