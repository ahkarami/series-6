package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Pi {
    private static BigDecimal pi = new BigDecimal(0);
    public synchronized static void add (BigDecimal bd) { pi = pi.add(bd); }
    public static BigDecimal getPi () { return pi; }
}
