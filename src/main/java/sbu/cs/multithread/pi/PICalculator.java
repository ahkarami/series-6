package sbu.cs.multithread.pi;

import java.math.RoundingMode;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class PICalculator {

    private static final ExecutorService executor = Executors.newFixedThreadPool(4);

    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */
    public String calculate(int floatingPoint) throws InterruptedException{
        int termNum = termNumFind(floatingPoint);

        for (int i = 0; i <= termNum; i++) {
            executor.submit(new PiSeries(i));
        }
        executor.shutdown();
        executor.awaitTermination(10000, TimeUnit.MILLISECONDS);

        return Pi.getPi().setScale(floatingPoint, RoundingMode.FLOOR).toString();
    }

    public int termNumFind (int floatingPoint) {
        int termNum = (int) ((2 * Math.pow(10, floatingPoint + 2)) + 1);
        return 10000;
    }
}
