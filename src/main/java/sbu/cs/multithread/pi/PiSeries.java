package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class PiSeries implements Runnable{

    private final BigDecimal FOUR = new BigDecimal(4);
    private final BigDecimal TWO = new BigDecimal(2);
    private int termNum;
    private int precision;

    public PiSeries(int termNum) {
        this.termNum = termNum;
    }

    public BigDecimal factorial (int k) {
        BigDecimal out = new BigDecimal(1);
        for (int i = 1; i <= k; i++) {
            out = out.multiply(new BigDecimal(i));
        }
        return out;
    }

    @Override
    public void run() {
        BigDecimal term = new BigDecimal(Math.pow(2, termNum + 1));
        term = term.multiply(factorial(termNum));
        term = term.multiply(factorial(termNum));
        term = term.divide(factorial((2 * termNum) + 1), new MathContext(10000, RoundingMode.HALF_UP));
        Pi.add(term);


    }
}
