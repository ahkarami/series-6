package sbu.cs.exception;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reader {

    /**
     * declare 2 Exception class. 1 for UnrecognizedCommand and 1 for NotImplementedCommand
     * iterate on function inputs and check for commands and throw exception when needed.
     *
     * @param args
     */
    public void readTwitterCommands(List<String> args) throws ApException {
        for (int i = 0; i < args.size(); i++) {
            if (!Util.getImplementedCommands().contains(args.get(i))) {
                if (Util.getNotImplementedCommands().contains(args.get(i))){
                    throw new NotImplementedCommand();
                } else {
                    throw new UnrecognizedCommand();
                }
            }
        }
    }

    /**
     * function inputs are String but odd positions must be integer parsable
     * a valid input is like -> "ap", "2", "beheshti", "3992", "20"
     * throw BadInput exception when the string is not parsable.
     *
     * @param args
     */
    public void read(String...args) throws BadInputException {

        boolean flag = true;
        for (int i = 1; i < args.length; i=i+2) {

            System.out.println(i);
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(args[i]);

            if (!matcher.matches()) {
                flag = false;
                break;
            }
        }

        if (!flag) {
            throw new BadInputException();
        }

    }
}
